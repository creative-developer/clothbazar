﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entities
{
   public class Product : BaseEntity
   {
      // Other Imp Fields Like ID, Name ... etc Inherit From BaseEntity Class

      public decimal Price { get; set; }
      public Category Category { get; set; }

   }
}
