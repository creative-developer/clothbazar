﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Entities
{
   public class Category : BaseEntity
   {
      // Other Imp Fields Like ID, Name ... etc Inherit From BaseEntity Class
      public List<Product> Products { get; set; }
   }
}
