﻿using ClothBazar.Entities;
using ClothBazar.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClothBazar.Web.Controllers
{
   public class CategoryController : Controller
   {
      private CategoriesService categorysServices = new CategoriesService();

      public ActionResult Index()
      {
         var categoriesList = categorysServices.GetCategories();

         return View(categoriesList);
      }


      // GET: Category
      [HttpGet]
      public ActionResult Create()
      {
         return View();
      }

      [HttpPost]
      public ActionResult Create(Category category)
      {
         categorysServices.SaveCategory(category);

         return RedirectToAction("Index");
      }


      [HttpGet]
      public ActionResult Edit(int id)
      {
         var category = categorysServices.GetCategory(id);

         return View(category);
      }

      [HttpPost]
      public ActionResult Edit(Category category)
      {
         categorysServices.UpdateCategory(category);

         return RedirectToAction("Index");
      }


      [HttpGet]
      public ActionResult Delete(int id)
      {
         var category = categorysServices.GetCategory(id);

         return View(category);
      }

      [HttpPost]
      public ActionResult Delete(Category category)
      {
         categorysServices.DeleteCategory(category.ID);

         return RedirectToAction("Index");
      }

   }
}